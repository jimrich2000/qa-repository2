# Ensure that in Windows you are running Git Bash

git add myfile.bat
git commit

# In vi press 'i' to Insert
myfile.bat added
# Press ESC then SHIFT+Z+Z to save and exit

git status
# Should be a nothing to commit message

touch newfile.bat
git add newfile.bat
git status
git commit -m "newfile.bat added"
git log --stat
